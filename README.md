# Kurt Lenfesty NZ Spring Boot Demo Playground

Demo of spring boot to understand its capabilities, in both client and server.

## Synopsis

Showcasing both client and server REST usage with Spring boot.

## Motivation

Spring boot is easy, but retrofitting a technical upgrade using Spring Boot can be difficult to demonstrate. This
playground showcases a simpler version of the same technology stack.

## Versioning

See the `build.gradle` file for the current jar version that will be generated.

## Installation

The artifacts are built using gradle and will deploy to a maven repository.

### Complete build
To do a complete build (including unit and code tests and javadoc):
```
gradle [clean] build
```

### Complete build with upgrade-preparation warnings
When gradle 5.x is release, some gradle features and certain build scripts will not work. In order to prepare for
this eventuality, builds can include the `warning-mode` to notify in advance of changes that will need to happen.
```
 gradle [clean] build --warning-mode all
```

### Running the spring-boot-server
The spring boot server can be run from the command-line:
```
gradle clean build spring-boot-demo-server:bootRun
```
Note that the application will be accessible from http://localhost:8082.

### Relevant URLs
The URLs where the different APIs are exposed are:
- http://localhost:8082 - home page with a variable that comes from `application.properties`.
- http://localhost:8082/alternate - a simple JSP with no forms or variables.
- http://localhost:8082/fleeb/fleeb-form - a simple form with a single modelAttribute.
- http://localhost:8082/fleeb/fleeb-multi-form - demonstrates a composite modelAttribute object (FleebMultiForm that
  contains a FleebCommand).
- http://localhost:8082/fleeb/fleeb-multi-post-form - demonstrates multiple forms on the same page that use different
  modelAttribute objects to post different forms.

### REST API
The REST API is tested using:
- http://localhost:8082/fleeb

### Running under IntelliJ
#### Lombok
In order for the Lombok annotations (which automatically generate the getters and setters using `@Getter` and `@Setter`
annotations), enable annotation processing in `Settings > Build > Compiler > Annotation Processors > 
[x] Enable annotation processing`. You may need to refresh the project after this change.
#### Run with module
When running the `PlaygroundServerApp` under IntelliJ, the `webapp/WEB-INF/jsp` JSPs will not get picked up unless
the run configuration is changed using `Run > Edit Configurations > PlaygroundServerApp > Environment > 
Working directory > $MODULE_WORKING_DIR$`.

## API Reference

To generate javadoc:
```
gradle javadoc
```

## Tests

To run unit and other tests:
```
gradle test
```

## Reports

### Unit test reports
Unit test coverage will be produced by
```
gradle test
```

## Contributors

See git commits to see who contributors are. Issues are tracked through the git repository issue tracker.

## License

None specified at this time. &copy; 2019 Kurt Lenfesty. All rights reserved.
