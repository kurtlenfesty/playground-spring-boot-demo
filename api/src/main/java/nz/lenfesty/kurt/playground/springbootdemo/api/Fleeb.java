package nz.lenfesty.kurt.playground.springbootdemo.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

@Component
@JsonIgnoreProperties(ignoreUnknown =  true)
public class Fleeb {
    int fleebAsInt;
    long fleebAsLong;
    String fleebAsString;

    public int getFleebAsInt() {
        return fleebAsInt;
    }

    public void setFleebAsInt(int fleebAsInt) {
        this.fleebAsInt = fleebAsInt;
    }

    public long getFleebAsLong() {
        return fleebAsLong;
    }

    public void setFleebAsLong(long fleebAsLong) {
        this.fleebAsLong = fleebAsLong;
    }

    public String getFleebAsString() {
        return fleebAsString;
    }

    public void setFleebAsString(String fleebAsString) {
        this.fleebAsString = fleebAsString;
    }

    @Override
    public String toString() {
        return "fleebAsInt=" + fleebAsInt + ", fleebAsLong=" + fleebAsLong + ", fleebAsString=" + fleebAsString;
    }
}
