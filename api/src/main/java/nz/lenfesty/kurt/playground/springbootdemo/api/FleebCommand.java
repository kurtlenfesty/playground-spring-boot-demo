package nz.lenfesty.kurt.playground.springbootdemo.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class FleebCommand {
    @NotBlank(message = "Command name cannot be blank.")
    private String commandName;

    @NotBlank(message = "Operation cannot be blank.")
    @Size(min = 2, max = 20)
    private String operation;
}
