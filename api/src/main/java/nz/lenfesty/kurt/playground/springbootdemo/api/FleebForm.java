package nz.lenfesty.kurt.playground.springbootdemo.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class FleebForm {
    @NotBlank(message = "Email must not be empty.")
    @Email(message = "Please enter a valid email address.")
    private String email;

    @NotBlank(message = "Password must not be empty.")
    private String password;

    @Max(message = "Maximum value is 22", value = 22)
    @Min(message = "Minimum value is -1", value = -1)
    private int numberOne;

    @PositiveOrZero(message = "Value must be >= 0")
    private int positiveOrZeroValue;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @FutureOrPresent(message = "Value must be present or future")
    private LocalDate nowOrFutureDate;
}
