package nz.lenfesty.kurt.playground.springbootdemo.client;

import nz.lenfesty.kurt.playground.springbootdemo.api.Fleeb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class PlaygroundChecker {
    private static final Logger log = LoggerFactory.getLogger(PlaygroundChecker.class);

    public void someSimpleChecks() {
        RestTemplate restTemplate = new RestTemplate();

        Integer getInt = restTemplate.getForObject("http://localhost:8082/fleeb/int/997", Integer.class);
        log.info("int returned=" + getInt);
        System.out.println("int returned=" + getInt);

        Long getLong = restTemplate.getForObject("http://localhost:8082/fleeb/long/6670", Long.class);
        log.info("long returned=" + getLong);
        System.out.println("long returned=" + getLong);

        String getString = restTemplate.getForObject("http://localhost:8082/fleeb/string/a-long-string", String.class);
        log.info("String returned=" + getString);
        System.out.println("String returned=" + getString);

        Fleeb fleeb = restTemplate.getForObject("http://localhost:8082/fleeb?fleebAsInt=123&fleebAsLong=99237&fleebAsString=this-is-a-long-string", Fleeb.class);
        log.info("fleeb returned=" + fleeb.toString());
        System.out.println("fleeb returned=" + fleeb.toString());

        HttpEntity<Fleeb> request = new HttpEntity<>(fleeb);
        ResponseEntity<Fleeb> adjustedFleebResponse = restTemplate.postForEntity("http://localhost:8082/fleeb", request, Fleeb.class);
        Fleeb adjustedFleeb = adjustedFleebResponse.getBody();
        log.info("adjustedFleeb returned=" + adjustedFleeb.toString());
        System.out.println("adjustedFleeb returned=" + adjustedFleeb.toString());
    }
}
