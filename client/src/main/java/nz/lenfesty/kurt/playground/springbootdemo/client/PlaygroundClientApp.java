package nz.lenfesty.kurt.playground.springbootdemo.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlaygroundClientApp {
    private static final Logger log = LoggerFactory.getLogger(PlaygroundClientApp.class);

    public static void main(String[] args) {
        PlaygroundChecker playgroundChecker = new PlaygroundChecker();
        playgroundChecker.someSimpleChecks();
    }

}
