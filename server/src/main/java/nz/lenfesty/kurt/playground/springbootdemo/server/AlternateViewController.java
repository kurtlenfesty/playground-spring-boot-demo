package nz.lenfesty.kurt.playground.springbootdemo.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

// Note that @Controller will return view names, whereas @RestController will return REST responses.
@Controller
public class AlternateViewController {
    @GetMapping(path = "/alternate")
    public String showAlternate() {
        return "alternate";
    }
}
