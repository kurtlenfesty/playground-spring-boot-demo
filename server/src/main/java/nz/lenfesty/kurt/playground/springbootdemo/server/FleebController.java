package nz.lenfesty.kurt.playground.springbootdemo.server;

import nz.lenfesty.kurt.playground.springbootdemo.api.Fleeb;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

// Note that @Controller will return view names, whereas @RestController will return REST responses.
@RestController
@RequestMapping("/fleeb")
public class FleebController {

    @GetMapping(path = "/int")
    public Integer asInt() {
        return new Random().nextInt();
    }

    @GetMapping(value = "/int/{int-value}")
    public Integer reflectInt(@PathVariable(value = "int-value") int intValue) {
        return intValue;
    }

    @GetMapping(path = "/long/{long-value}")
    public Long reflectLog(@PathVariable(value = "long-value") long longValue) {
        return longValue;
    }

    @GetMapping(path = "/string/{string-value}")
    public String reflectString(@PathVariable(value = "string-value") String stringValue) {
        return stringValue;
    }

    @GetMapping
    public Fleeb reflectFleeb(Fleeb fleeb) {
        return fleeb;
    }

    @PostMapping
    public Fleeb adjustFleeb(@RequestBody Fleeb fleeb) {
        fleeb.setFleebAsInt(fleeb.getFleebAsInt() + 10000);
        fleeb.setFleebAsLong(fleeb.getFleebAsLong() + 1000000L);
        fleeb.setFleebAsString("An ADDITIONAL " + fleeb.getFleebAsString());

        return fleeb;
    }
}
