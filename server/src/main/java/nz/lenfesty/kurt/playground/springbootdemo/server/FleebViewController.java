package nz.lenfesty.kurt.playground.springbootdemo.server;

import nz.lenfesty.kurt.playground.springbootdemo.api.FleebCommand;
import nz.lenfesty.kurt.playground.springbootdemo.api.FleebForm;
import nz.lenfesty.kurt.playground.springbootdemo.api.FleebMultiForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;

// Note that @Controller will return view names, whereas @RestController will return REST responses.
@Controller
@RequestMapping("/fleeb")
public class FleebViewController {
    @GetMapping(path = "/fleeb-form")
    public String showFleebForm(@ModelAttribute("fleebForm") FleebForm fleebForm) {
        fleebForm.setEmail("my@email.address");
        fleebForm.setPositiveOrZeroValue(22);
        fleebForm.setNowOrFutureDate(LocalDate.now());

        return "fleeb-form";
    }

    @PostMapping(path = "/fleeb-form")
    public String postFleebForm(@Valid @ModelAttribute("fleebForm") FleebForm fleebForm,
                                // Note that the BindingResult must be IMMEDIATELY after the parameter it validates.
                                BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            session.setAttribute("fleebed", "FAILED");
        } else {
            System.out.println("fleebForm: email=" + fleebForm.getEmail() + ", password=" + fleebForm.getPassword() +
                    ", numberOne=" + fleebForm.getNumberOne() + ", positiveOrZeroValue=" + fleebForm.getPositiveOrZeroValue() +
                    ", nowOrFutureDate=" + fleebForm.getNowOrFutureDate());
            session.setAttribute("fleebed", "OK");
        }

        return "fleeb-form";
    }

    @GetMapping(path = "/fleeb-multi-form")
    public String showFleebMultiForm(@ModelAttribute("fleebMultiForm") FleebMultiForm fleebMultiForm) {
        FleebCommand fleebCommand = new FleebCommand();
        fleebCommand.setCommandName("verify");
        fleebCommand.setOperation("TEST");
        fleebMultiForm.setFleebCommand(fleebCommand);

        fleebMultiForm.setEmail("my@email.address");
        fleebMultiForm.setPositiveOrZeroValue(22);
        fleebMultiForm.setNowOrFutureDate(LocalDate.now());

        return "fleeb-multi-form";
    }

    @PostMapping(path = "/fleeb-multi-form")
    public String postFleebMultiForm(@Valid @ModelAttribute("fleebMultiForm") FleebMultiForm fleebMultiForm,
                                     // Note that the BindingResult must be IMMEDIATELY after the parameter it validates.
                                     BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            session.setAttribute("fleebed", "FAILED");
        } else {
            FleebCommand fleebCommand = fleebMultiForm.getFleebCommand();
            System.out.println("fleebCommand: commandName=" + fleebCommand.getCommandName() + ", operation=" +
                    fleebCommand.getOperation());

            System.out.println("fleebForm: email=" + fleebMultiForm.getEmail() + ", password=" + fleebMultiForm.getPassword() +
                    ", numberOne=" + fleebMultiForm.getNumberOne() + ", positiveOrZeroValue=" + fleebMultiForm.getPositiveOrZeroValue() +
                    ", nowOrFutureDate=" + fleebMultiForm.getNowOrFutureDate());
            session.setAttribute("fleebed", "OK");
        }

        return "fleeb-multi-form";
    }

    // Note that this mapping for the fleeb-multi-post-form.jsp populates two modelAttributes: fleebMultiForm and
    // fleebCommand, which are used by different forms on the JSP page.
    @GetMapping(path = { "/fleeb-multi-post-form", "/fleeb-multi-post-form-one", "/fleeb-multi-post-form-command-only" })
    public ModelAndView showFleebMultiPostFormOne() {
        ModelAndView modelAndView = new ModelAndView("fleeb-multi-post-form");

        FleebCommand fleebCommand = new FleebCommand();
        fleebCommand.setCommandName("verify-multi-post-form-one");
        fleebCommand.setOperation("TEST");

        FleebMultiForm fleebMultiForm = new FleebMultiForm();
        fleebMultiForm.setFleebCommand(fleebCommand);

        fleebMultiForm.setEmail("my@email.address");
        fleebMultiForm.setPositiveOrZeroValue(22);
        fleebMultiForm.setNowOrFutureDate(LocalDate.now());

        modelAndView.getModelMap().addAttribute("fleebMultiForm", fleebMultiForm);

        FleebCommand fleebCommandAlone = new FleebCommand();
        fleebCommandAlone.setCommandName("verify-multi-post-form-command-only");
        fleebCommandAlone.setOperation("SEPARATE");
        modelAndView.getModelMap().addAttribute("fleebCommand", fleebCommandAlone);

        return modelAndView;
    }

    @PostMapping(path = "/fleeb-multi-post-form-one")
    public ModelAndView postFleebMultiPostFormOne(@Valid @ModelAttribute("fleebMultiForm") FleebMultiForm fleebMultiForm,
                                                  // Note that the BindingResult must be IMMEDIATELY after the parameter it validates.
                                                  BindingResult bindingResult,
                                                  // Note that the fleebCommand comes after BindingResult, as we
                                                  // don't want it validated.
                                                  @ModelAttribute("fleebCommand") FleebCommand fleebCommand,
                                                  HttpSession session) {
        System.out.println("fleebCommand: commandName=" + fleebCommand.getCommandName() + ", operation=" +
                fleebCommand.getOperation());

        System.out.println("fleebForm: email=" + fleebMultiForm.getEmail() + ", password=" + fleebMultiForm.getPassword() +
                ", numberOne=" + fleebMultiForm.getNumberOne() + ", positiveOrZeroValue=" + fleebMultiForm.getPositiveOrZeroValue() +
                ", nowOrFutureDate=" + fleebMultiForm.getNowOrFutureDate());

        if (bindingResult.hasErrors()) {
            //model.addAttribute("fleebCommand", new FleebCommand());
            session.setAttribute("fleebed", "FAILED");
        } else {
            session.setAttribute("fleebed", "OK");
        }

        // It doesn't matter if you include the unposted object in the ModelAndView or not -- it's still going to be
        // empty because the form that gets posted is not including it.
        ModelAndView modelAndView = new ModelAndView("fleeb-multi-post-form");
        //modelAndView.getModelMap().addAttribute("fleebMultiForm", fleebMultiForm);
        modelAndView.getModelMap().addAttribute("fleebCommand", fleebCommand);

        return modelAndView;
    }

    @PostMapping(path = "/fleeb-multi-post-form-command-only")
    public ModelAndView postFleebMultiPostFormCommandOnly(@Valid @ModelAttribute("fleebCommand") FleebCommand fleebCommand,
                                                          // Note that the BindingResult must be IMMEDIATELY after the parameter it validates.
                                                          BindingResult bindingResult,
                                                          // Note that the fleebMultiForm comes after BindingResult, as we
                                                          // don't want it validated.
                                                          @ModelAttribute("fleebMultiForm") FleebMultiForm fleebMultiForm,
                                                          HttpSession session) {
        System.out.println("fleebCommand: commandName=" + fleebCommand.getCommandName() + ", operation=" +
                fleebCommand.getOperation());

        System.out.println("fleebForm: email=" + fleebMultiForm.getEmail() + ", password=" + fleebMultiForm.getPassword() +
                ", numberOne=" + fleebMultiForm.getNumberOne() + ", positiveOrZeroValue=" + fleebMultiForm.getPositiveOrZeroValue() +
                ", nowOrFutureDate=" + fleebMultiForm.getNowOrFutureDate());

        if (bindingResult.hasErrors()) {
            //model.addAttribute("fleebMultiForm", new FleebMultiForm());
            session.setAttribute("fleebed", "FAILED");
        } else {
            session.setAttribute("fleebed", "OK");
        }

        // It doesn't matter if you include the unposted object in the ModelAndView or not -- it's still going to be
        // empty because the form that gets posted is not including it.
        ModelAndView modelAndView = new ModelAndView("fleeb-multi-post-form");
        //modelAndView.getModelMap().addAttribute("fleebMultiForm", fleebMultiForm);
        modelAndView.getModelMap().addAttribute("fleebCommand", fleebCommand);

        return modelAndView;
    }

}
