package nz.lenfesty.kurt.playground.springbootdemo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "nz.lenfesty.kurt.playground.springbootdemo" })
public class PlaygroundServerApp {

    public static void main(String[] args) {
		SpringApplication.run(PlaygroundServerApp.class, args);
	}

}
