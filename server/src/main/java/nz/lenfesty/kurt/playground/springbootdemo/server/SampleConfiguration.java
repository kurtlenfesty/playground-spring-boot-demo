package nz.lenfesty.kurt.playground.springbootdemo.server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SampleConfiguration {
    @Value("${base.property.one}")
    private String basePropertyOne;

    @Value("${base.property.two}")
    private String basePropertyTwo;

    @Value("${local.property.one}")
    private String localPropertyOne;

    @Value("${property.from.local}")
    private String propertyFromLocal;

    @Bean
    public SampleObject sampleObjectBeanOne() {
        SampleObject sampleObject = new SampleObject();
        sampleObject.doStuff("sampleObjectBeanOne", "${base.property.one}", basePropertyOne);

        return sampleObject;
    }

    @Bean
    public SampleObject sampleObjectBeanTwo() {
        SampleObject sampleObject = new SampleObject();
        sampleObject.doStuff("sampleObjectBeanTwo", "${base.property.two}", basePropertyTwo);

        return sampleObject;
    }

    @Bean
    public SampleObject sampleObjectLocalOne() {
        SampleObject sampleObject = new SampleObject();
        sampleObject.doStuff("sampleObjectLocalOne", "${local.property.one}", localPropertyOne);

        return sampleObject;
    }

    @Bean
    public SampleObject sampleObjectFromLocal() {
        SampleObject sampleObject = new SampleObject();
        sampleObject.doStuff("sampleObjectFromLocal", "${property.from.local}", propertyFromLocal);

        return sampleObject;
    }
}
