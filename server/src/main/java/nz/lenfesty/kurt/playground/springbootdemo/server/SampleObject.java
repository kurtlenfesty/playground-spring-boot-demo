package nz.lenfesty.kurt.playground.springbootdemo.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(SampleObject.class);

    public void doStuff(String beanName, String key, String value) {
        LOGGER.error("* * * doStuff START beanName={}", beanName);
        LOGGER.trace("doStuff trace - key={}, value={}", key, value);
        LOGGER.debug("doStuff debug - key={}, value={}", key, value);
        LOGGER.info("doStuff info - key={}, value={}", key, value);
        LOGGER.warn("doStuff warn - key={}, value={}", key, value);
        LOGGER.error("doStuff error - key={}, value={}", key, value);
        LOGGER.error("* * * doStuff END beanName={}", beanName);
    }

}
