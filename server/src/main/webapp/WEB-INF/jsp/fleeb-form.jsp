<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>Fleeb Form</title>
</head>
<body>
<div class="jumbotron">
    <h1>Spring Boot 2 + JSP Demo</h1>
</div>
<div class="container">
    <div class="row">
        <form:form method="POST" modelAttribute="fleebForm" action="/fleeb/fleeb-form">
            <div class="form-group">
                <label for="email">E-Mail</label>
                <form:input path="email" cssClass="form-control"/>
                <form:errors path="email" cssStyle="color: red"/>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <!-- showPassword="true" means the password is retained even if the form fails validation -->
                <form:password showPassword="true" path="password" cssClass="form-control"/>
                <form:errors path="password" cssStyle="color: red"/>
            </div>
            <div class="form-group">
                <label for="numberOne">Number-One</label>
                <form:input path="numberOne" cssClass="form-control"/>
                <form:errors path="numberOne" cssStyle="color: red"/>
            </div>
            <div class="form-group">
                <label for="positiveOrZeroValue">Positive Or Zero Value</label>
                <form:input path="positiveOrZeroValue" cssClass="form-control"/>
                <form:errors path="positiveOrZeroValue" cssStyle="color: red"/>
            </div>
            <div class="form-group">
                <label for="nowOrFutureDate">Now or Future Date (yyyy-MM-dd)</label>
                <form:input path="nowOrFutureDate" cssClass="form-control"/>
                <form:errors path="nowOrFutureDate" cssStyle="color: red"/>
            </div>
            <form:button class="btn btn-primary">Submit Button</form:button>
        </form:form>
    </div>
    <!--- check login status and display message -->
    <div class="row">
        <% Object fleebed = session.getAttribute("fleebed");
            if ("OK".equals(fleebed)) { %>
        <div class="alert alert-success" role="alert">
            Congratulations! Fleebed successfully.
        </div>
        <% }
            if ("FAILED".equals(fleebed)) { %>
        <div class="alert alert-danger" role="alert">
            Fleebed failed. Please try again!!!
        </div>
        <% } %>
    </div>
</div>
</body>
</html>
